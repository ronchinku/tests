﻿using JAG.DevTest2019.ServiceHost.WebAPI;
using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace JAG.DevTest2019.ServiceHost
{
    class Program
    {
        private static IDisposable _WebApiServiceHost;

        static void Main(string[] args)
        {
            Console.WriteLine($"Starting WebAPI");
            int apiPort = 8099;
            string apiHost = "http://localhost";
            string url = $"{apiHost}:{apiPort}";

            _WebApiServiceHost = WebApp.Start<WebApiStartup>(url);
            Console.WriteLine($"WebAPI hosted on {url}");

            var startTimeSpan = TimeSpan.Zero;
            var periodTimeSpan = TimeSpan.FromSeconds(60);
            var timer = new System.Threading.Timer((e) =>
            {
                PollLeads();
            }, null, startTimeSpan, periodTimeSpan);

            Console.WriteLine($"Press enter to exit");
            Console.ReadLine();
        }

        private static void PollLeads()
        {
            SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["localDB"].ConnectionString);

            string selectQuery = "Select Firstname,LastName, IsNew, LeadId From Lead Where IsNew = @IsNew";
            SqlCommand cmd = new SqlCommand(selectQuery, con);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@IsNew", 0);

            string updateQuery = "UPDATE Lead SET IsNew = @IsNew WHERE LeadId=@LeadId";
            SqlCommand updateLeadCmd = new SqlCommand(updateQuery, con);
            updateLeadCmd.Parameters.AddWithValue("@IsNew", 1);
            updateLeadCmd.Parameters.Add("@LeadId", SqlDbType.BigInt);

            Console.WriteLine($"Polling at: " + DateTime.Now.ToLocalTime());

            try
            {
                con.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Console.WriteLine("Lead: " + reader.GetValue(0) + " " + reader.GetValue(1) + " is new.");
                        updateLeadCmd.Parameters["@LeadId"].Value = reader.GetValue(3);
                        updateLeadCmd.ExecuteNonQuery();
                    }
                    reader.Close();
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine("Error: " + e.ToString());
            }
            finally
            {
                con.Close();
            }           
        }
    }
}
