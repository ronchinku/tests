﻿using JAG.DevTest2019.Model;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace JAG.DevTest2019.LeadService.Controllers
{
    public class LeadController : ApiController
    {
        [HttpPost]
        [ResponseType(typeof(LeadResponse))]
        public HttpResponseMessage Post(Lead request)
        {
            LeadResponse response = new LeadResponse()
            {
                LeadId = 10000000 + new Random().Next(),
                IsCapped = false,
                Messages = new[] { "Success from WebAPI " },
                IsSuccessful = true,
                IsDuplicate = false
            };

            InsertIntoDB(request, ref response);

            Console.WriteLine($"Lead received {request.FirstName} {request.Surname}");

            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        private int? InsertIntoDB(Lead leadToInsert, ref LeadResponse response)
        {
            int? ID = null;

            SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["localDB"].ConnectionString);
            string leadQuery = "INSERT INTO Lead (LeadId,TrackingCode, FirstName, LastName, ContactNumber, Email, ReceivedDateTime, IsDuplicate, IsSuccessful, IsCapped) VALUES(@LeadId, @TrackingCode, @Name, @Surname, @ConNumber, @Email, @ReceivedDateTime, @IsDuplicate, @IsSuccessful, @IsCapped)";
            SqlCommand leadcmd = new SqlCommand(leadQuery, con);
            leadcmd.Parameters.AddWithValue("@LeadId", response.LeadId);
            leadcmd.Parameters.AddWithValue("@Name", leadToInsert.FirstName);
            leadcmd.Parameters.AddWithValue("@Surname", leadToInsert.Surname);
            leadcmd.Parameters.AddWithValue("@ConNumber", leadToInsert.ContactNumber);
            leadcmd.Parameters.AddWithValue("@Email", leadToInsert.EmailAddress);
            leadcmd.Parameters.AddWithValue("@ReceivedDateTime", DateTime.Now.ToLocalTime());
            leadcmd.Parameters.AddWithValue("@TrackingCode", "12345");
            leadcmd.Parameters.Add("@IsSuccessful", SqlDbType.Bit);

            string leadparamQuery = "INSERT INTO LeadParameter (LeadParameterId, LeadId, Name, Value) VALUES(@LeadParameterId, @LeadId, @Name, @Value)";
            SqlCommand leadparamcmd = new SqlCommand(leadparamQuery, con);
            leadparamcmd.Parameters.AddWithValue("@LeadParameterId", response.LeadId);
            leadparamcmd.Parameters.AddWithValue("@LeadId", response.LeadId);
            leadparamcmd.Parameters.AddWithValue("@Name", leadToInsert.FirstName);
            leadparamcmd.Parameters.AddWithValue("@Value", "A value");

            string duplicateQuery = "SELECT LeadId FROM Lead WHERE ContactNumber = @ConNumber OR Email = @Email";
            SqlCommand cmd = new SqlCommand(duplicateQuery, con);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@ConNumber", leadToInsert.ContactNumber);
            cmd.Parameters.AddWithValue("@Email", leadToInsert.EmailAddress);

            string trackQuery = "SELECT COUNT(LeadId), TrackingCode FROM Lead WHERE TrackingCode = @TrackingCode GROUP BY TrackingCode HAVING COUNT(LeadId) > 10";
            SqlCommand trackcmd = new SqlCommand(trackQuery, con);
            trackcmd.CommandType = CommandType.Text;
            trackcmd.Parameters.AddWithValue("@TrackingCode", "12345");
            bool isDupOrCapped = false;

            try
            {
                con.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        leadcmd.Parameters.AddWithValue("@IsDuplicate", 1);
                        response.Messages[0] = "duplicate on [email/ contact number] " + Environment.NewLine;
                        isDupOrCapped = true;
                        response.IsDuplicate = true;
                    }
                    else
                    {
                        leadcmd.Parameters.AddWithValue("@IsDuplicate", 0);
                        leadcmd.Parameters["@IsSuccessful"].Value = 1;
                    }
                }

                using (SqlDataReader reader = trackcmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            response.Messages[0] += reader.GetValue(0) + " lead recieved today, code " + reader.GetValue(1) + " is capped.";
                        }
                        leadcmd.Parameters.AddWithValue("@IsCapped", 1);
                        response.IsCapped = true;
                        isDupOrCapped = true;
                    }
                    else
                    {
                        leadcmd.Parameters.AddWithValue("@IsCapped", 0);
                    }
                }

                leadcmd.Parameters["@IsSuccessful"].Value = isDupOrCapped ? 0 : 1;
                response.IsSuccessful = !isDupOrCapped;

                leadcmd.ExecuteNonQuery();
                leadparamcmd.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                Console.WriteLine("Error: " + e.ToString());
            }
            finally
            {
                con.Close();
            }
            return ID;
        }



    }

}
