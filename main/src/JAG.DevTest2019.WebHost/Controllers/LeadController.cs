﻿using JAG.DevTest2019.Host.Models;
using JAG.DevTest2019.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace JAG.DevTest2019.Host.Controllers
{

    public class LeadController : Controller
    {
        public ActionResult Index()
        {

            ViewData["randomValue"] = GetRandomValue();
            return View(new LeadViewModel());
        }

        public ActionResult SubmitLead(LeadViewModel model)
        {
            HttpResponseMessage response = PostLead(model);

            LeadViewModel result = new LeadViewModel()
            {
                Results = new LeadResultViewModel() { }
            };

            string jsonReply = response.Content.ReadAsStringAsync().Result;
            LeadResponse leadResponse = JsonConvert.DeserializeObject<LeadResponse>(jsonReply);
            result.Results.LeadId = leadResponse.LeadId;
            result.Results.Message = leadResponse.Messages[0];
            result.Results.IsSuccessful = leadResponse.IsSuccessful;

            ViewData["randomValue"] = GetRandomValue();
            return View("Index", result);
        }

        [HttpGet]
        public JsonResult RandomValue()
        {
            Random r = new Random();
            var randomValue = r.Next(6000001).ToString();
            ViewData["randomValue"] = randomValue;
            return Json(randomValue, JsonRequestBehavior.AllowGet);
        }

        private string GetRandomValue()
        {
            Random r = new Random();
            return r.Next(6000001, 999999999).ToString();
        }
        private HttpResponseMessage PostLead(LeadViewModel model)
        {
            Lead apiLead = new Lead();
            apiLead.FirstName = model.FirstName;
            apiLead.Surname = model.Surname;
            apiLead.ContactNumber = model.ContactNumber;
            apiLead.EmailAddress = model.EmailAddress;

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:8099/");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            string postContent = JsonConvert.SerializeObject(apiLead);
            HttpContent content = new StringContent(postContent, Encoding.UTF8, "application/json");

            return client.PostAsync("api/Lead/Post" + postContent, content).Result;
        }
    }
}