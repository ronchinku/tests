#JAG Method software developer assessment
## Answers

### 1. SEO (5min)

1) add here

2) add here

3) add here

4) add here

### 2. Responsive (15m)

1) add here

2) add here

3) add here

4) add here


### 3. Validation (15m)
Add any special implemetation instructions here.
* ContactNumber: Should be 10 digits in length and start with a 0 and no spaces.

### 4. JavaScript (20m)
Add any special implemetation instructions here.

### 5. Ajax calls (30m)
Add any special implemetation instructions here.

### 6. Call a REST webservice (25m)
Add any special implemetation instructions here.

Make sure that the WebHost calls the ServiceHost via REST.

### 7. ADO.Net (40m)
Add any SQL schema changes here

### 8. Poll DB (15m)
Add any SQL schema changes here
--Added bit column 'IsNew'
USE [JAG2019]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lead](
	[LeadId] [bigint] NOT NULL,
	[TrackingCode] [varchar](20) NOT NULL,
	[FirstName] [nvarchar](255) NOT NULL,
	[LastName] [nvarchar](255) NOT NULL,
	[ContactNumber] [varchar](20) NOT NULL,
	[Email] [varchar](150) NULL,
	[ReceivedDateTime] [datetime] NOT NULL,
	[IPAddress] [varchar](50) NULL,
	[UserAgent] [varchar](500) NULL,
	[ReferrerURL] [varchar](500) NULL,
	[IsDuplicate] [bit] NULL,
	[IsCapped] [bit] NULL,
	[IsSuccessful] [bit] NULL,
	[IsNew] [bit] NOT NULL DEFAULT 0,
 CONSTRAINT [PK_Lead] PRIMARY KEY CLUSTERED 
(
	[LeadId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LeadParameter]    Script Date: 2018/11/12 4:48:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LeadParameter](
	[LeadParameterId] [bigint] NOT NULL,
	[LeadId] [bigint] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Value] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_LeadParameter] PRIMARY KEY CLUSTERED 
(
	[LeadParameterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LeadParameter]  WITH CHECK ADD  CONSTRAINT [FK_LeadParameter_Lead] FOREIGN KEY([LeadId])
REFERENCES [dbo].[Lead] ([LeadId])
GO
ALTER TABLE [dbo].[LeadParameter] CHECK CONSTRAINT [FK_LeadParameter_Lead]
GO


Make changes ServiceHost

### 9. SignalR (40m)
Add any SQL schema changes here

### 10. Data Analysis (30m)

1) Total Profit
**Answer**

**SQL**
SELECT SUM(ISNULL([Earnings],0) - ISNULL([Cost], 0)) AS [Total Profit]
FROM [JAG2019].[dbo].[LeadDetail]

2) Total Profit (Earnings less VAT)
**Answer**

**SQL**
SELECT SUM(ISNULL([Earnings],0)) - SUM(ISNULL([Cost], 0))*0.85 AS [Total Profit]
FROM [JAG2019].[dbo].[LeadDetail]

3) Profitable campaigns
**Answer**

**SQL**
SELECT SUM(ISNULL([Earnings],0)) - SUM(ISNULL([Cost], 0))*0.85 AS [Total Profit], c.CampaignName
FROM [JAG2019].[dbo].[LeadDetail] l , [JAG2019].[dbo].[Campaign] c 
WHERE l.CampaignId = c.CampaignId
GROUP BY c.CampaignName
HAVING  SUM(ISNULL([Earnings],0)) - SUM(ISNULL([Cost], 0))*0.85 > 0

4) Average conversion rate
**Answer**

**SQL**
 
 SELECT AVG(rawConv) AS 'conv' FROM  (
	SELECT ((SELECT CAST(COUNT(*) AS float) FROM [JAG2019].[dbo].[LeadDetail] WHERE IsSold = 1) 
	/ (SELECT CAST(COUNT(*) as float) FROM [JAG2019].[dbo].[LeadDetail] WHERE IsAccepted = 1)) as 'rawConv'
	FROM  [JAG2019].[dbo].[LeadDetail]

  ) AS t

5) Pick 2 clients based on Profit & Conversion rate & Why?
**Answer**

**SQL**
`Select....`
